const XLSX = require('xlsx');


exports.writeFile = function(path, data, {keys=Object.keys(data[0]), sheetName="Sheet1", bookType='xls'} = {}){
  
  let dataArray = data.map(r=>objectToDataArray(r, keys))
  //Add Header
  dataArray.unshift(keys)
  const wb = XLSX.utils.book_new(), ws = XLSX.utils.aoa_to_sheet(dataArray);
  XLSX.utils.book_append_sheet(wb, ws, sheetName);
  XLSX.writeFile(wb, path, {bookType});
}

function objectToDataArray(obj, keys=Object.keys(obj)){
  return keys.map(key=>obj[key])
}

exports.writeFile('/tmp/atlTest.xls', [{a:1,b:2,c:3}])