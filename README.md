# atl-jsonCSV

Simple library to convert objects to xls.


## Install

```shell
npm install git+https://bitbucket.org/ATLTed/atl-json-xls.git#master
```

## Example Usage

```javascript
const jsonXLS = require('atl-json-xls');

const exampleArray = [{a:"a1",b:"b1",c:"b1"}, {a:"a2",b:"b2",c:"b2"}];

jsonXLS.writeFile('/tmp/data1.xls', exampleArray);

jsonXLS.writeFile('/tmp/data2.xls', exampleArray, {keys:['b', 'a']});

//BookType = 'xlsx' | 'xlsm' | 'xlsb' | 'xls' | 'xla' | 'biff8' | 'biff5' | 'biff2' | 'xlml' | 'ods' | 'fods' | 'csv' | 'txt' | 'sylk' | 'html' | 'dif' | 'rtf' | 'prn' | 'eth';
jsonXLS.writeFile('/tmp/data3.xlsx', exampleArray, {bookType:"xlsx"});

jsonXLS.writeFile('/tmp/data4.xls', exampleArray, {sheetName:"Any Name"});
```